/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import DAO.CategoriaDAO;
import DAO.ProductoDAO;
import entidades.Producto;

/**
 *
 * @author Luis Valenzuela
 */
public class ProductoDAOTest {
    
    public static void main(String[] args){
        ProductoDAO pDAO = new ProductoDAO();
        
        // el for recibe un producto que viene del listado
        for (Producto p: pDAO.listar()) {
            System.out.println(p.getNombre());
        }
        
        CategoriaDAO cDAO = new CategoriaDAO();
        for (Producto p: pDAO.listar()) {
            System.out.println(p.getNombre());
        }
    }
}
